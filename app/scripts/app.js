'use strict';

/**
 * @ngdoc overview
 * @name angularTestApp
 * @description
 * # angularTestApp
 *
 * Main module of the application.
 */
angular
  .module('angularTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider,$httpProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'scripts/login/views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
        .when('/access_token=:access_token', {
            template: '',
            controller: function ($location,$rootScope) {
                var hash = $location.path().substr(1);

                var splitted = hash.split('&');
                var params = {};

                for (var i = 0; i < splitted.length; i++) {
                    var param  = splitted[i].split('=');
                    var key    = param[0];
                    var value  = param[1];
                    params[key] = value;
                    $rootScope.accesstoken=params;
                }

                $location.path('/home');
            }
        }).when('/home',{
            templateUrl : 'scripts/home/views/home.html',
            controller : 'HomeCtrl',
            controllerAs : 'home'
        }).otherwise({
            redirectTo: '/'
        });
  });
