(function () {
    'use strict';

    angular.module('angularTestApp').constant('APP_CONSTANT',{
        'CLIENT_ID' : '699487829926-8gf1jdm057e2ltf4cug1tgg5bu7t8sv5.apps.googleusercontent.com',
        'REDIRECT_URL' : 'http://localhost:9000',
        'AUTH_URL' : 'https://accounts.google.com/o/oauth2/auth?scope=email',
        'API_BASE_URL' : 'http://itunes.apple.com/search?term='
    });
})();
