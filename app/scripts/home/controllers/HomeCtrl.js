(function(){
    'use strict';

    angular.module('angularTestApp').controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$rootScope', 'MediaResource'];
    return HomeCtrl;
    function HomeCtrl ($scope, $rootScope, MediaResource) {
        console.log($rootScope.accesstoken);
        $scope.availableMedia = MediaResource.getAvailableMedia();
        $scope.results = [];
        $scope.isHideList = false;
        $scope.previewUrl = '';
        $scope.mediaType = ''

        $scope.getMediaList = getMediaList;
        $scope.getPreviewUrl = getPreviewUrl;

        function getMediaList() {
            var params = {
                term : 'U2',
                section : 'media',
                type : ''
            };

            if ($scope.selectedMedia) {
                params.type = $scope.selectedMedia;
                MediaResource.get(params, function(response){
                    console.log(response);
                    $scope.results = response.results;
                }, function (e) {

                });
            }
        }

        function getPreviewUrl(url, type) {
            $scope.isHideList = true;
            $scope.previewUrl = url;
            $scope.mediaType = type;
        }
    }
})();
