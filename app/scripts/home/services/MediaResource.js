(function() {
    'use strict';

    angular.module('angularTestApp').factory('MediaResource', MediaResource);

    MediaResource.$inject = ['$resource', 'APP_CONSTANT'];

    function MediaResource($resource, APP_CONSTANT) {
        var endPoint = APP_CONSTANT.API_BASE_URL + ':term &:section=:type',
            resource =
                $resource (
                   endPoint,
                   {
                       term: '@term',
                       section: '@section',
                       type: '@type'
                   }
           );

        var Media = {
            query: function (params, success, error) {
                    resource.query(params,
                        function (response) {
                            if (typeof success !== 'undefined' && success instanceof Function)
                                success.call(undefined, response);
                        },
                        function (response) {
                            if (typeof error !== 'undefined' && error instanceof Function)
                                error.call(undefined, response);
                        });
                },

                get: function (params, success, error) {
                    resource.get(params,
                        function (response) {
                            if (typeof success !== 'undefined' && success instanceof Function)
                                success.call(undefined, response);
                        },
                        function (response) {
                            if (typeof error !== 'undefined' && error instanceof Function)
                                error.call(undefined, response);
                        });
                },
                getAvailableMedia : function () {
                    var mediaList = [{
                        name : 'Music',
                        type : 'music'
                    }, {
                        name : 'Music Video',
                        type : 'musicVideo'
                    }];

                    return mediaList;
                }
        };

        return Media;
    }
})();
