(function () {
    'use strict';

    angular.module('angularTestApp').directive('previewMedia', previewMedia);

    previewMedia.$inject = ['$sce'];
    function previewMedia($sce) {
        function link (scope, elem, attr) {
            scope.newUrl = '';
            scope.isPreview = false;

            scope.closeMedia = closeMedia;

            function closeMedia() {
                scope.isPreview = false;
                scope.isHideList = false;
            }



            var urlListener = scope.$watch('previewUrl', function (n, o) {
                if (n !== o) {
                        scope.newUrl = $sce.trustAsResourceUrl(n);
                        scope.isPreview = true;
                        if (scope.type === 'song') {
                            scope.settings = "audio/mp4"
                        } else {
                            scope.settings = "video/x-m4v"
                        }
                }
            });

            scope.$on('destroy', function (){
                urlListener();
            });
        }
        return {
            link : link,
            scope : {
                'previewUrl' : '=',
                'type' : '=',
                'isHideList' : '='
            },
            templateUrl : 'scripts/home/views/preview.html'
        }
    }
})();
