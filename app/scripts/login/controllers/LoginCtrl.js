(function() {
    'use strict';

    angular.module('angularTestApp').controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope', 'APP_CONSTANT'];

    return LoginCtrl;
    function LoginCtrl($scope, APP_CONSTANT) {
        $scope.login = {};

        $scope.submit = submit;

        function submit() {
            //if($scope.login.email && $scope.login.password) {
                var url = APP_CONSTANT.AUTH_URL
                    + '&client_id=' + APP_CONSTANT.CLIENT_ID + '&redirect_uri='
                    + APP_CONSTANT.REDIRECT_URL + '&response_type=' + 'token';

                window.location.replace(url);
            //}
        }
    }
})();
